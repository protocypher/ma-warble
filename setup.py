from setuptools import setup


setup(
      name="Warble",
      version="0.1.0",
      packages=["warble"],
      url="https://bitbucket.org/protocypher/ma-warble",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="An ID3 tag editor for MP3s."
)

