# Warble

**ID3 Tag Editor**

Warble is a basic, text based, ID3 tag editor for MP3 files that uses the
`cmd.Cmd` framework. It allows the user to see and edit tags for an MP3 file
or files in batch.

